# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=xmrig
pkgver=6.6.1
pkgrel=0
pkgdesc="XMRig is a high performance Monero (XMR) miner"
url="https://xmrig.com/"
arch="aarch64 x86 x86_64" # officially supported by upstream
license="GPL-3.0-or-later"
options="!check" # No test suite from upstream
makedepends="cmake libmicrohttpd-dev libuv-dev openssl-dev hwloc-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/xmrig/xmrig/archive/v$pkgver.tar.gz"

case "$CARCH" in
	aarch64*) CMAKE_CROSSOPTS="-DARM_TARGET=8"; makedepends="$makedepends linux-headers" ;;
esac

build() {
	cmake -B build $CMAKE_CROSSOPTS -DCMAKE_BUILD_TYPE=None \
		-DWITH_CUDA=OFF \
		-DWITH_OPENCL=OFF

	make -C build
}

package() {
	install -Dm 755 build/xmrig $pkgdir/usr/bin/xmrig

	install -Dm 644 -t "$pkgdir"/usr/share/doc/$pkgname/ README.md
}

sha512sums="1782e120398b1e68129dc807d7aaa8b29b0c09ca3fc0c847a86820a4368e6a360449d1c0a86ecc25a0384d106d39e0d59b2be243f5c15ab00022c049768ff415  xmrig-6.6.1.tar.gz"
